#!/bin/sh
#
# Copyright (c) 2019 James Anderson-Pole <james@pole.net.nz>
#

#
# TODO:
# - Install Apache conf files from Includes/ folder.
# - Install smb4.conf file
# - Configure email sending to go via FastMail's SMTP server.

#
# Set configurations.
#

DIR_HOME='/home/james/'
FILES_HOME='.cshrc .vimrc'
PACKAGES='apache24 en-freebsd-doc firefox git hs-ShellCheck jdupes lynis
	openssh-portable samba47 sudo unifi5 vim-console xdm xfce xorg'
SERVICES_DISABLE='sendmail sshd'
SERVICES_ENABLE='apache24 dbus openssh ntpd ntpdate samba_server unifi'

#
# Set commands.
#

FREEBSD_UPDATE='freebsd-update'
GIT='git'
INSTALL='install -v'
PKG='pkg'
PKG_INSTALL="${PKG} install -y"
SERVICE='service'
SUDO='sudo'

#
# Define functions for printing messages.
#

remark() {
	printf "\033[0;36m>> %s\033[0m\n" "${1}"
}

fail() {
	printf '\033[1;31m\n'
	echo '=========='
	echo "${1}"
	echo '=========='
	printf '\033[0m\n'
	exit 1
}

finish() {
	printf '\033[0;32m\n'
	echo '=========='
	echo "${1}"
	echo '=========='
	printf '\033[0m\n'
	exit 0
}

notice() {
	printf '\033[0;33m\n'
	echo '=========='
	echo "${1}"
	echo '=========='
	printf '\033[0m\n'
	sleep 1
}

#
# Define functions for actions.
#

install_root() {
	if diff "${1}" "${2}"
	then
		remark "Not overwriting file ${2} as it is identical."
	else
		${INSTALL} -o root -g wheel -m 0440 "${1}" "${2}" \
			|| fail "Could not install file to ${2}."
	fi
}

install_user() {
	if diff "${1}" "${2}"
	then
		remark "Not overwriting file ${2} as it is identical."
	else
		${INSTALL} -o james -g james -m 0440 "${1}" "${2}" \
			|| fail "Could not install file to ${2}."
	fi
}

#
# Fetch updates.
#

notice 'Fetching updates.'

${FREEBSD_UPDATE} fetch \
	|| fail 'Could not fetch updates.'

#
# Install updates.
#

notice 'Installing updates.'

${FREEBSD_UPDATE} install \
	|| fail 'Could not install updates.'

#
# Make sure pkg is installed.
#

notice 'Making sure pkg is installed.'

${PKG} bootstrap -y \
	|| fail 'Could not bootstrap pkg.'

#
# Update pkg database.
#

notice 'Updating pkg database.'

${PKG} update \
	|| fail 'Could not update pkg database.'

#
# Install packages.
#

notice 'Installing packages.'

${PKG_INSTALL} ${PACKAGES} \
	|| fail 'Could not install packages.'

#
# Configure terminals.
#

notice 'Configuring terminals.'

install_root ttys /etc/ttys

remark 'Restarting the init process (PID 1).'

kill -HUP 1 \
	|| fail 'Could not restart the init process.'

#
# Configure sudo.
#

notice 'Configuring sudo.'

install_root wheel /usr/local/etc/sudoers.d/wheel

#
# Configuring Git for james@.
#

notice 'Configuring Git for james@.'

remark 'Setting Git user name.'

${SUDO} -u james ${GIT} config --global user.name "James Pole" \
	|| fail 'Could not set Git user name.'

remark 'Setting Git user email.'

${SUDO} -u james ${GIT} config --global user.email "james@pole.net.nz" \
	|| fail 'Could not set Git user email.'

#
# Installing files for james@.
#

notice 'Installing files for james@.'

for file in ${FILES_HOME}
do
	install_user "${file}" "${DIR_HOME}${file}"
done

#
# Stop and disable services that are not needed.
#

notice 'Stopping and disabling services that are not needed.'

for service in ${SERVICES_DISABLE}
do
	if ${SERVICE} "${service}" enabled
	then
		${SERVICE} "${service}" stop \
			|| fail "Could not stop ${service}."
		${SERVICE} "${service}" disable \
			|| fail "Could not disable ${service}."
	else
		remark "Skipping disabled service ${service}."
	fi
done

#
# Enable and start services that are needed.
#

notice 'Enabling and starting services that are needed.'

for service in ${SERVICES_ENABLE}
do
	if ${SERVICE} "${service}" enabled
	then
		remark "Skipping enabled service ${service}."
	else
		${SERVICE} "${service}" enable \
			|| fail "Could not enable ${service}."
		${SERVICE} "${service}" start \
			|| fail "Could not start ${service}."
	fi
done

#
# Announce completion and recommend restart of machine.
#

finish 'All done! Please restart your machine.'
